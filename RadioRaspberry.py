############################
# Imports                  #
############################

import threading
import RPi.GPIO as GPIO
from time import sleep
import signal
import sys
sys.path.append('lib')
from Button import *
import os

class GUI(threading.Thread):
        def __init__(self):
                self.playing = False
                self.stop = False

                self.bouton1 = Button( 4 , self.Button1Pressed )
                self.bouton1.start()

                self.bouton2 = Button( 17, self.Button2Pressed )
                self.bouton2.start()

                self.bouton3 = Button( 18, self.Button3Pressed )
                self.bouton3.start()

                self.bouton4 = Button( 27, self.Button4Pressed )
                self.bouton4.start()

                self.bouton5  = Button( 22, self.Button5Pressed )
                self.bouton5.start()

                self.bouton6 = Button( 23, self.Button6Pressed )
                self.bouton6.start()

                self.bouton7 = Button( 24, self.Button7Pressed )
                self.bouton7.start()

                threading.Thread.__init__(self)

        def Stop(self):
                #print("Appel de GUI.Stop()")
                os.system("mpc pause")
                self.stop = True
#               self.lcd.Stop()
                self.bouton1.Stop()
                self.bouton2.Stop()
                self.bouton3.Stop()
                self.bouton4.Stop()
                self.bouton5.Stop()
                self.bouton6.Stop()
                self.bouton7.Stop()

                threading.Thread.__init__(self)


        def run(self):
                while self.stop == False:
                        sleep(0.1)

        def SignalStop(self, signal, frame):
                self.Stop()
                sys.exit(0)

        def Button5Pressed(self):
                os.system("mpc prev")
                os.system("mpc play")

        def Button6Pressed(self):
                os.system("mpc next")
                os.system("mpc play")
                self.playing = True

        def Button1Pressed(self):

                os.system("mpc play 1 ")


        def Button2Pressed(self):
                 os.system("mpc play 5 ")


        def Button3Pressed(self):
                os.system("mpc play 8 ")

        def Button4Pressed(self):
                os.system("mpc play 11")

        def Button7Pressed(self):
                self.Stop()
                os.system("sudo halt")


if __name__ == '__main__':
        gui = GUI()
        gui.start()
        signal.signal(signal.SIGINT, gui.SignalStop)
        signal.signal(signal.SIGQUIT, gui.SignalStop)
        signal.signal(signal.SIGTERM, gui.SignalStop)
        while 1 :
                sleep ( 60 )
        gui.Stop()


